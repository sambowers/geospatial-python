
Introduction to Geospatial Python
=================================

Welcome to this very short introduction to using Python for geospatial applications.

We will start with the very basics of Python, and then look at how Python can be used to process, analyse and interpret geospatial data.

Our aim is to introduce you to some of the fundamental concepts of Python. We do not expect you to be able to program fluently in Python by the time you are done (that will take a lot more practice), but we hope to give you an overview of how Python works, and how it might be able to help you in future.  

We will aim to build up this documentation over time to give you a record of the methods we've covered. Take note of the URL, these notes may be something that you'll refer back to in future.

Aims
----

By the end of this tutorial, we hope that you will be able to:

* Write simply Python scripts that use different data types (``strings``, ``integers``, ``floats``, ``boolean``) and how to group them (``lists``), and flow control statements (``for``,  ``if``, ``while``).
* Perform basic data manipulation in Python
* Understand how arrays are used to process image data in NumPy
* Use Matplotlib to make scatter plots and display images

Instructions
------------

* Work through each line of code, and make it run on your own PC
* Do not copy-paste. You'll learn a lot by entering each command manually
* When you encounter an error, in the first instance try and work out the issue yourself. If you're still stuck, do ask for some pointers
* Don't worry about making mistakes, you can't break Python
* Using Google to help with exercises is encouraged
* Go at your own pace; we don't expect you to reach the end of this tutorial today

Contents
--------

.. toctree::
   :maxdepth: 2
   :numbered:

   setup.rst
   python101.rst
   numpy.rst
   matplotlib.rst
   to_be_continued.rst


Search
------

* :ref:`search`

.. Indices and tables
.. ------------------

.. * :ref:`genindex`
.. * :ref:`modindex

.. image:: images/UoE_Horizontal_Logo_CMYK_v1_160215.jpg
   :scale: 20 %

.. image:: images/ltsi_logo.jpg
