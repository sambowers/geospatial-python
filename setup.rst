
Setup instructions
==================

Installing Python
-----------------

Before we do anything, we'll need to set up Python to work on your PC. The instructions that follow refer to installation using a Linux PC.

We'll be working with Python 2.7, which is currently the most widely used version of Python. There is a newer version of Python (3.x), which has some small improvements to its syntax, but is not yet widely supported by the scientific packages (or 'modules') we'll be using. 

`Anaconda`_ is one of a number of Python 'distributions', which include the Python interpreter and a range of Python packages. We will be using Anaconda as it is one of the most straightforward to install, and it comes with most of the packages you'll be likely to use.

.. _Anaconda: https://www.continuum.io/downloads

There are many ways to run Python, including in an Integrated Development Environment (IDE) (e.g. `Spyder'_, `PyCharm`_). These are pieces of software that include features for advanced editing, interactive testing, and debudding. We won't be using an IDE as part of this tutorial, but be aware that there exist many IDEs, and you may find in time that you find an IDE that suits you. Or, if you're like me, you may find that you prefer to write scripts in a simple text editor and prefer not to use an IDE at all.

.. _Spyder: https://pythonhosted.org/spyder/
.. _PyCharm: https://www.jetbrains.com/pycharm/

To download and install Anaconda, ``cd`` to a location you want to save Anaconda, and execute the following::
    
    wget https://repo.anaconda.com/archive/Anaconda2-5.1.0-Linux-x86_64.sh
    chmod +x Anaconda2-5.1.0-Linux-x86_64.sh
    ./Anaconda2-5.1.0-Linux-x86_64.sh

Accept all the default options in the install.


One important Python module that Anaconda does not come bundled with is ``GDAL``. We use GDAL to process georeferenced imagery (i.e. satellite data) using Python. To install ``gdal``, execute the command::
    
    conda install -c anaconda gdal

Using Python
------------

There are two ways to execute Python code:

* By writing code directly into the interpreter window using Python's 'interactive mode' (great for learning)
* By writing scripts in a text editor and then running in an interpreter window (better for data processing)

To open Python's interactive mode, open a terminal window and type ``ipython``. You could also run ``python``, but we recommend using IPython (Interactive Python) for it's user friendliness. You should see something that looks like:

.. image:: images/ipython_terminal.png

To run a script, you can type ``run scipt_name.py`` into the IPython window. Alternatively, from the Linux command line you can execute::
    
    python script_name.py

We'll be using Python both interactively and to run scripts in this tutorial. 


Using Python in interactive mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Where we want you to enter code directly into the interpreter window in interactive mode, code snippets will be preceded by a ``>>>``.

Try typing ``import this`` into the 'interpreter window' followed by the ``enter`` key. You should see the following:

.. code-block:: python

    >>> import this
    The Zen of Python, by Tim Peters
    
    Beautiful is better than ugly.
    Explicit is better than implicit.
    Simple is better than complex.
    Complex is better than complicated.
    Flat is better than nested.
    Sparse is better than dense.
    Readability counts.
    Special cases aren't special enough to break the rules.
    Although practicality beats purity.
    Errors should never pass silently.
    Unless explicitly silenced.
    In the face of ambiguity, refuse the temptation to guess.
    There should be one-- and preferably only one --obvious way to do it.
    Although that way may not be obvious at first unless you're Dutch.
    Now is better than never.
    Although never is often better than *right* now.
    If the implementation is hard to explain, it's a bad idea.
    If the implementation is easy to explain, it may be a good idea.
    Namespaces are one honking great idea -- let's do more of those!

Using Python to run scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Where we want you to write a script in a text editor and run it, the ``>>>`` symbol will be absent. There are a number of text editors included with most Linux distributions (e.g. ``vim``, ``gedit``, ``kate``). For this tutorial we'll use ``gedit``.

Open a new terminal, ``cd`` to your working directory and run ``gedit``. Type the following into the 'script window', save it as ``helloworld.py``:

.. code-block:: python

    print 'Hello World!'

Then, in the interpreter window type ``run helloworld.py``. You should see the following:

.. code-block:: python

    >>> run helloworld.py
    Hello World!

Don't move on any further until you've been able to run the ``import this`` command in the interpreter window and managed to run the 'Hello World' script. Ask if you need assistance.
