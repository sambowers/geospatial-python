
To be continued...
==================

Further reading
---------------

Learning to write useful scripts in Python takes a lot of time and commitment. The best way to learn is by doing.

If you would like to learn more about Python independently, there are loads of free online resources. Here are the sites that I've found particularly useful in learning to use Python:

* `Learn Python the hard way`_: an excellent online Python course
* `Codecademy`_: another well-known Python course
* `Stack Overflow`_: for diagnosing errors
* `Google`_: seriously!

.. _Learn Python the hard way: https://learnpythonthehardway.org/
.. _Codecademy: https://www.codecademy.com/learn/python
.. _Stack Overflow: https://stackoverflow.com/questions/tagged/python
.. _Google: https://www.google.com


Stay in touch
-------------

Please do email either Sam (sam.bowers@ed.ac.uk) or Simone (simone-vaccari@ltsi.co.uk) if you'd like any assistance or any further tips. We'll be very happy to help.

Happy coding!

.. figure:: images/xkcd_python.png
    
    Souce: `XKCD`_.

.. _XKCD: https://xkcd.com/353/


